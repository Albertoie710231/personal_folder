/*************************************************************************
Name of the module:
	SHIFT_R.sv
Description:
	This module is shift register
Version:
	1.0
Author:
	Alberto Vargas   <ie710231@iteso.mx>
	Isaac Vázquez 	<ie703092@iteso.mx>
Date: 
	17/09/2021
*************************************************************************/

module shift_l #
(
    parameter DW = 4,
    parameter OFFSET = 1
)
(
	// Input - Value to be shifted if enb is set	
	input logic[DW-1:0] 	datain,
	
	// Output - Shifted value to the right
	output logic[DW-1:0] dataout
);
//	temporary register
logic[DW-1:0] temp_register;

always_comb begin
    temp_register = datain << OFFSET;
end

// Assign the temporary register to the output
assign dataout = temp_register;
	
endmodule
