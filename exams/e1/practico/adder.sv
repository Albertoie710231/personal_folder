/*************************************************************************
Name of the module:
	adder.sv
Description:
	This module is a common adder for calculate operations
Version:
	1.0
Author:
		Alberto Vargas   <ie710231@iteso.mx>
	Isaac Vázquez 	<ie703092@iteso.mx>
Date:
	11/2/2021
*************************************************************************/
 module adder #
(
    parameter DW=4
)
(
	// Input - Value to be shifted if enb is set
	input logic[DW-1:0] 	datain_x,
	input logic[DW-1:0] 	datain_y,

	// Output - Shifted value to the right
	output logic[2*DW-1:0] dataout
);
//	temporary register
logic[DW-1:0] temp_register;

always_comb begin
    temp_register = datain_x + datain_y;
end

// Assign the temporary register to the output
assign dataout = temp_register;

endmodule
