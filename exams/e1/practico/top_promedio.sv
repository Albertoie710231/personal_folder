module top_promedio #
(
    parameter N=4
)
(
    input logic[N-1:0] datain,
    input bit clk,
    input logic rst,
    input logic enb,
    output logic[N-1:0] dataout
);

logic[8*N-1:0] w_reg_8n_to_shift;
logic[8*N-1:0] w_shift_to_reg;
logic[2*N-1:0] w_adder_1;
logic[2*N-1:0] w_adder_2;
logic[2*N-1:0] w_adder_3;
logic[2*N-1:0] w_adder_4;
logic[4*N-1:0] w_adder_5;
logic[4*N-1:0] w_adder_6;
logic[8*N-1:0] w_adder_7;

pipo #
(
    .DW(8*N)
)
reg_8n
(
    .clk(clk),
    .rst(rst),
    .enb(enb),
    .clean('0),
    .inp({w_shift_to_reg[8*N-1:N],datain}),
    .out(w_reg_8n_to_shift)
);

shift_l #
(
    .DW(8*N),
    .OFFSET(N)
)
shift_to_reg
(
    .datain(w_reg_8n_to_shift),
    .dataout(w_shift_to_reg)
);

adder #
(
    .DW(N)
)
adder_1
(
    .datain_x(w_reg_8n_to_shift[N-1:0]),
    .datain_y(w_reg_8n_to_shift[2*N-1:N]),
    .dataout(w_adder_1)
);

adder #
(
    .DW(N)
)
adder_2
(
    .datain_x(w_reg_8n_to_shift[3*N-1:2*N]),
    .datain_y(w_reg_8n_to_shift[4*N-1:3*N]),
    .dataout(w_adder_2)
);

adder #
(
    .DW(N)
)
adder_3
(
    .datain_x(w_reg_8n_to_shift[5*N-1:4*N]),
    .datain_y(w_reg_8n_to_shift[6*N-1:5*N]),
    .dataout(w_adder_3)
);

adder #
(
    .DW(N)
)
adder_4
(
    .datain_x(w_reg_8n_to_shift[7*N-1:6*N]),
    .datain_y(w_reg_8n_to_shift[8*N-1:7*N]),
    .dataout(w_adder_4)
);

adder #
(
    .DW(2*N)
)
adder_5
(
    .datain_x(w_adder_1),
    .datain_y(w_adder_2),
    .dataout(w_adder_5)
);

adder #
(
    .DW(2*N)
)
adder_6
(
    .datain_x(w_adder_3),
    .datain_y(w_adder_4),
    .dataout(w_adder_6)
);

adder #
(
    .DW(4*N)
)
adder_7
(
    .datain_x(w_adder_5),
    .datain_y(w_adder_6),
    .dataout(w_adder_7)
);

shift_r #
(
    .DW(8*N),
    .OFFSET(3)
)
shift_to_out
(
    .datain(w_adder_7),
    .dataout(dataout)
);

endmodule
