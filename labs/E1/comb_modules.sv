//Header 
// Coder: 		Alberto Vargas Garrido
// Description: 	This is a simple multiplexer
// Date: 		25 August 2021

module comb_modules #(
	parameter 	DW 	= 4, // Crean instancia, esto se sobreescribe
	parameter 	DW2 	= 1 // Este localparam no se puede sobreescribir al instanciar
) ( // Este nombre debe ser exactamente igual al nombre del archivo .sv
// bit: 0 o 1
input bit				cclk, 
// Asynchronous low active reset	
input bit				arst_n,
//
input logic	[DW-1:0] 		summand1,	//Logic contiene 4 estados: 0, 1, Z(Alta impedancia), X(Valor no importa)
input logic	[DW2-1:0] 		summand2,
output logic	[fn_max(DW,DW2):0]	suma		// Maximo(Val1, Val2)+1
);
logic [fn_max(DW,DW2):0] suma_comb;
logic [fn_max(DW,DW2):0] suma_ff;

//Procedural blocks
always_comb begin
	suma_comb = summand1 + summand2;
end

// Estado en la industria pos edge
always_ff@( posedge cclk or negedge arst_n) begin

// Esto es un registro de entrada y salida paralela, con reset asuncron activo
// bajo.
	if(!arst_n)
		suma_ff <= '0;
	else
		suma_ff <= suma_comb;
end

assign suma = suma_ff;

function automatic integer fn_max (
input integer DW,
input integer DW2
);
	integer tmp;
	tmp = 0;
	if(DW>DW2)
		tmp = DW;
	else
		tmp = DW2;

	return tmp;
endfunction

endmodule
