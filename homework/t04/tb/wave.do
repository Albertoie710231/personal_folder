onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /FIFO_TB/wclk
add wave -noupdate /FIFO_TB/rclk
add wave -noupdate /FIFO_TB/wrst
add wave -noupdate /FIFO_TB/rrst
add wave -noupdate /FIFO_TB/pop
add wave -noupdate /FIFO_TB/push
add wave -noupdate -radix decimal /FIFO_TB/DataInput
add wave -noupdate /FIFO_TB/full
add wave -noupdate /FIFO_TB/empty
add wave -noupdate -radix decimal /FIFO_TB/DataOutput
add wave -noupdate -divider {FIFO DATA}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[16]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[15]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[14]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[13]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[12]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[11]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[10]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[9]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[8]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[7]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[6]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[5]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[4]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[3]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[2]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[1]}
add wave -noupdate -radix decimal {/FIFO_TB/DUV/dp_ram/ram[0]}
add wave -noupdate -divider {RPTR EMPTY}
add wave -noupdate /FIFO_TB/DUV/rptr_empty/rempty
add wave -noupdate -radix decimal /FIFO_TB/DUV/rptr_empty/raddr
add wave -noupdate -radix decimal /FIFO_TB/DUV/rptr_empty/rptr
add wave -noupdate -radix decimal /FIFO_TB/DUV/rptr_empty/rq2_wptr
add wave -noupdate /FIFO_TB/DUV/rptr_empty/rinc
add wave -noupdate /FIFO_TB/DUV/rptr_empty/rclk
add wave -noupdate /FIFO_TB/DUV/rptr_empty/rrst_n
add wave -noupdate -divider {WPTR FULL}
add wave -noupdate /FIFO_TB/DUV/wptr_full/waddr
add wave -noupdate /FIFO_TB/DUV/wptr_full/wptr
add wave -noupdate /FIFO_TB/DUV/wptr_full/wq2_rptr
add wave -noupdate /FIFO_TB/DUV/wptr_full/winc
add wave -noupdate /FIFO_TB/DUV/wptr_full/wclk
add wave -noupdate /FIFO_TB/DUV/wptr_full/wrst_n
add wave -noupdate -divider MEM
add wave -noupdate /FIFO_TB/DUV/dp_ram/mem_if/we_a
add wave -noupdate /FIFO_TB/DUV/dp_ram/mem_if/data_a
add wave -noupdate /FIFO_TB/DUV/dp_ram/mem_if/rd_data_a
add wave -noupdate /FIFO_TB/DUV/dp_ram/mem_if/wr_addr_a
add wave -noupdate /FIFO_TB/DUV/dp_ram/mem_if/rd_addr_b
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {15 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 326
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {97 ps}
