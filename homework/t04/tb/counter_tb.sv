`timescale 1ns/1ps

localparam PERIOD = 2;

module counter_tb;

parameter N= 10;

bit clk;
logic rst;
logic enb;
logic enb_cont;
logic[N-1:0] out;

grey_counter gcounter
(
	.clk(clk),
	.rst(rst),
	.enb(enb),
	.count(out)
);

initial begin
	clk = 1;
#0.1	rst = 0;
	enb = 0;
#PERIOD		rst = 1;
#(3*PERIOD)	enb = 1;
	$monitor ("T=%0t		rst=%0b		out=0x%0h", $time, rst, out);
#100
	$stop;

end

always #(PERIOD/2) clk <= ~clk;

endmodule
