
timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.
import sdp_dc_ram_pkg::*;
import fifo_pkg::*;

module FIFO_TB;

	// Parameter Declarations
parameter Word_Length = W_DATA;
parameter Depth_Of_FIFO = W_ADDR;

// Input Ports
bit wclk;
bit rclk;
bit wrst;
bit rrst;
bit pop;
bit push;
bit [Word_Length-1:0] DataInput;

// Output Ports
bit full;
bit empty;
bit [Word_Length-1:0] DataOutput;



/********************* Device Under Verification **************/
top_fifo

#(
	// Parameter Declarations
	.DSIZE(Word_Length),
	.ASIZE(Depth_Of_FIFO)
)
DUV
(
    .wclk(wclk),
    .rclk(rclk),
    .wrst_n(wrst),
    .rrst_n(rrst),
    .push(push),
    .pop(pop),
    .wdata(DataInput),

    .wfull(full),
    .rempty(empty),
    .rdata(DataOutput)
);

/**************************************************************************/
	
/******************** Stimulus *************************/
initial // Clock generator
  begin
    wclk = 1'b0;
    forever #2 wclk = !wclk;
  end

initial // Clock generator
  begin
    rclk = 1'b0;
    forever #5 rclk = !rclk;
  end
/*----------------------------------------------------------------------------------------*/
initial begin /*Reset*/
	#0 wrst = 1'b0;
	#3 wrst = 1'b1;
end

initial begin /*Reset*/
	#0 rrst = 1'b0;
	#3 rrst = 1'b1;
end
/*----------------------------------------------------------------------------------------*/
initial begin 
	# 0 pop  = 1'b0;
	# 80 pop  = 1'b1;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
end 
/*----------------------------------------------------------------------------------------*/
initial begin 
	# 0  push = 0;
	# 10  push = 1'b1;
	# 4  push = 1'b1;
	# 4  push = 1'b1;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
end
/*----------------------------------------------------------------------------------------*/ 
initial begin
	# 0  DataInput = 1'b0;
	# 8  DataInput = 8;
	# 4  DataInput = 7;
	# 4  DataInput = 6;
	# 4  DataInput = 5;
	# 4  DataInput = 0;
	# 4  DataInput = 4;
	# 4  DataInput = 0;
	# 4  DataInput = 3;
	# 4  DataInput = 0;
	# 4  DataInput = 2;
	# 4  DataInput = 0;
	# 4  DataInput = 1;
end


/*--------------------------------------------------------------------*/



endmodule
 
 
/*************************************************************/
/*************************************************************/

 
