`ifndef FIFO_PKG
    `define FIFO_PKG
package fifo_pkg;

localparam W_DATA_F = 4;

typedef logic [W_DATA_F-1:0] counter_bus_t;
typedef logic [W_DATA_F:0] counter_temp_bus_t;

endpackage
`endif
