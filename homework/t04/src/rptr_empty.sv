module rptr_empty #(parameter ADDRSIZE = 4)
  (output reg                rempty,

   output     [ADDRSIZE-1:0] raddr,

   output reg [ADDRSIZE  :0] rptr,

   input      [ADDRSIZE  :0] rq2_wptr,

   input                     rinc, rclk, rrst_n);

  wire [ADDRSIZE:0] rgraynext;
  bit rempty_val;
  //-------------------
  // GRAYSTYLE2 pointer

  //-------------------

  //always @(posedge rclk or negedge rrst_n)

    //if (!rrst_n) {rbin, rptr} <= 0;

    //else         {rbin, rptr} <= {rbinnext, rgraynext};
  //Memory read-address pointer (okay to use binary to address memory)
  //assign raddr     = rbin[ADDRSIZE-1:0];
  //assign rbinnext  = rbin + (rinc & ~rempty);
  //assign rgraynext = (rbinnext>>1) ^ rbinnext;

logic[ADDRSIZE+1:0] q;
logic[ADDRSIZE+1:0] no_ones_below;
logic q_msb;
int i;
int j;

always_comb begin
	q_msb = q[ADDRSIZE+1] | q[ADDRSIZE];
	no_ones_below[0] = 1'b1;
	for (j = 1; (ADDRSIZE+1) >= j ; j++) begin
		no_ones_below[j] = no_ones_below[j-1] & ~q[j-1];
	end
end

always_ff@(posedge rclk or negedge rrst_n) begin
	if (!rrst_n) begin
		q[0] <= 1'b1;
		q[ADDRSIZE+1:1] <= '0;
	end
	else if(rinc && ~rempty) begin
		q[0] <= ~q[0];
		for (i = 1; (ADDRSIZE+1) >= i ; i++) begin
			q[i] <= (q[i] ^ (q[i-1]) & no_ones_below[i-1]);
		end
		q[ADDRSIZE+1] <= q[ADDRSIZE+1] ^ (q_msb & no_ones_below[ADDRSIZE]);
	end
end

assign {rgraynext, raddr, rptr} = {q[ADDRSIZE+1:1],q[ADDRSIZE:1],q[ADDRSIZE+1:1]};

  //---------------------------------------------------------------
  // FIFO empty when the next rptr == synchronized wptr or on reset

  //---------------------------------------------------------------

  assign rempty_val = (rgraynext == rq2_wptr);
  always @(posedge rclk or negedge rrst_n)
    if (!rrst_n) rempty <= 1'b1;

    else         rempty <= rempty_val;

endmodule
