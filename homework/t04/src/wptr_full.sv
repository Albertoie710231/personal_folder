module wptr_full  #(parameter ADDRSIZE = 4)
  (output reg                wfull,

   output     [ADDRSIZE-1:0] waddr,

   output reg [ADDRSIZE  :0] wptr,

   input      [ADDRSIZE  :0] wq2_rptr,

   input                     winc, wclk, wrst_n);

wire [ADDRSIZE:0] wgraynext;
bit wfull_val;
// GRAYSTYLE2 pointer
logic[ADDRSIZE+1:0] q;
logic[ADDRSIZE+1:0] no_ones_below;
logic q_msb;
int i;
int j;

always_comb begin
	q_msb = q[ADDRSIZE+1] | q[ADDRSIZE];
	no_ones_below[0] = 1'b1;
	for (j = 1; (ADDRSIZE+1) >= j ; j++) begin
		no_ones_below[j] = no_ones_below[j-1] & ~q[j-1];
	end
end

always_ff@(posedge wclk or negedge wrst_n) begin
	if (!wrst_n) begin
		q[0] <= 1'b1;
		q[ADDRSIZE+1:1] <= '0;
	end
	else if(winc && ~wfull) begin
		q[0] <= ~q[0];
		for (i = 1; (ADDRSIZE+1) >= i ; i++) begin
			q[i] <= (q[i] ^ (q[i-1]) & no_ones_below[i-1]);
		end
		q[ADDRSIZE+1] <= q[ADDRSIZE+1] ^ (q_msb & no_ones_below[ADDRSIZE]);
	end
end

assign {wgraynext,wptr,waddr} = {q[ADDRSIZE+1:1],q[ADDRSIZE+1:1],q[ADDRSIZE:1]};
  //------------------------------------------------------------------
  // Simplified version of the three necessary full-tests:

  // assign wfull_val=((wgnext[ADDRSIZE]    !=wq2_rptr[ADDRSIZE]  ) &&

  //                   (wgnext[ADDRSIZE-1]  !=wq2_rptr[ADDRSIZE-1]) &&

  //                   (wgnext[ADDRSIZE-2:0]==wq2_rptr[ADDRSIZE-2:0]));

  //------------------------------------------------------------------

  assign wfull_val = (wgraynext=={~wq2_rptr[ADDRSIZE:ADDRSIZE-1],

                                   wq2_rptr[ADDRSIZE-2:0]});
  always @(posedge wclk or negedge wrst_n)
    if (!wrst_n) wfull  <= 1'b0;

    else         wfull  <= wfull_val;

endmodule
