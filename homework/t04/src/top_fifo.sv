import sdp_dc_ram_pkg::*;
import fifo_pkg::*;

module top_fifo #
(
    parameter DSIZE = W_DATA,
    parameter ASIZE = W_ADDR
)
(
    input bit wclk,
    input bit rclk,
    input logic wrst_n,
    input logic rrst_n,
    input logic push,
    input logic pop,
    input logic[DSIZE-1:0] wdata,

    output logic wfull,
    output logic rempty,
    output logic[DSIZE-1:0] rdata
);

sdp_dc_ram_if dp_ram_interface();

assign dp_ram_interface.data_a = wdata;

wire[ASIZE-1:0] waddr, raddr;
wire[ASIZE:0]   wptr, rptr, wq2_rptr, rq2_wptr;

sync_r2w #
(
    ASIZE
)
sync_r2w
(
    .wq2_rptr(wq2_rptr),
    .rptr(rptr),
    .wclk(wclk),
    .wrst_n(wrst_n)
);

sync_w2r #
(
    ASIZE
)
sync_w2r
(
    .rq2_wptr(rq2_wptr),
    .wptr(wptr),
    .rclk(rclk),
    .rrst_n(rrst_n)
);

rptr_empty #
(
    ASIZE
)
rptr_empty
(
    .rempty(rempty),
    .raddr(raddr),
    .rptr(rptr),
    .rq2_wptr(rq2_wptr),
    .rinc(pop),
    .rclk(rclk),
    .rrst_n(rrst_n)
);

assign dp_ram_interface.rd_addr_b = raddr;

wptr_full  #
(
    ASIZE
)
wptr_full
(
    .wfull(wfull),
    .waddr(waddr),
    .wptr(wptr),
    .wq2_rptr(wq2_rptr),
    .winc(push),
    .wclk(wclk),
    .wrst_n(wrst_n)
);

assign dp_ram_interface.wr_addr_a = waddr;
assign dp_ram_interface.we_a = ~wfull & push;

sdp_dc_ram dp_ram
(
    .clk_a(wclk),
    .clk_b(rclk),
    .mem_if(dp_ram_interface)
);

assign rdata = dp_ram_interface.rd_data_a;

endmodule
