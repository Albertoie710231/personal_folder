`ifndef GREY_COUNTER
 `define GREY_COUNTER

import fifo_pkg::*;
 
module grey_counter
(
	input bit clk,
	input logic rst,
	input logic enb,
	output counter_bus_t count
);

counter_temp_bus_t q;
counter_temp_bus_t no_ones_below;
logic q_msb;
int i;
int j;

always_comb begin
	q_msb = q[W_DATA_F] | q[W_DATA_F-1];	
	no_ones_below[0] = 1'b1;
	for (j = 1; W_DATA_F >= j ; j++) begin
		no_ones_below[j] = no_ones_below[j-1] & ~q[j-1];
	end
end

always_ff@(posedge clk or negedge rst) begin
	if (!rst) begin	
		q[0] <= 1'b1;
		q[W_DATA_F:1] <= '0;
	end
	else if(enb) begin
		q[0] <= ~q[0];
		for (i = 1; W_DATA_F >= i ; i++) begin
			q[i] <= (q[i] ^ (q[i-1]) & no_ones_below[i-1]);
		end
		q[W_DATA_F] <= q[W_DATA_F] ^ (q_msb & no_ones_below[W_DATA_F-1]);
	end
	else begin
		q[0] <= 1'b1;
		q[W_DATA_F:1] <= 1'b0;
	end
end

assign count = q[W_DATA_F:1]; 

endmodule
`endif
