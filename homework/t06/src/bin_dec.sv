module bin_dec #
(
    parameter W_DATA = 8,
    parameter W_N_DATA = $clog2(W_DATA)
)
(
    input logic[W_DATA-1:0] in_a,
    output logic[W_N_DATA-1:0] out_dec
);

logic[W_N_DATA-1:0] temp_pos = '0;

always_comb begin
    temp_pos = '0;
    for(int i = '0; (1'b0 == in_a[i]) && (i < W_DATA-1) ;i++) begin
        temp_pos++;
    end
end

assign out_dec = temp_pos;

endmodule
