`ifndef RR_FSM_SV
    `define RR_FSM_SV

import rr_pkg::*;

module rr_fsm
(
    input logic clk,
    input logic rst,
    //input logic finished,
    input logic in_hold_grant,
    input logic[W_DATA-1:0] mask,
    input logic[W_DATA-1:0] request,
    output logic out_in_reg,
    output logic out_mux_mask,
    output logic out_mask_reg,
    output logic out_out_reg
);

rr_state_e current_state, next_state;

always_comb begin
    case(current_state)
    IDLE: begin
        if('0 != request)
            next_state = START;
        else
            next_state = IDLE;
    end
    START: begin
        if('0 == request)
            next_state = IDLE;
        else if('1 == in_hold_grant)
            next_state = GRANT;
        else if('0 == in_hold_grant)
            next_state = HOLD;
        else
            next_state = START;
    end
    GRANT: begin
        if('0 == request)
            next_state = IDLE;
        else if(('0 != mask) && ('0 == in_hold_grant))
            next_state = HOLD;
        else if('0 == mask)
            next_state = START;
        else
            next_state = GRANT;
    end
    HOLD: begin
        if('0 == request)
            next_state = IDLE;
        else if(('0 != mask) && ('1 == in_hold_grant))
            next_state = GRANT;
        else if(('0 == mask) && ('1 == in_hold_grant))
            next_state = START;
        else
            next_state = HOLD;
    end
    default: begin
        next_state = IDLE;
    end
	 endcase
end

always_ff@(posedge clk or negedge rst) begin
    if(!rst)
        current_state <= IDLE;
    else
        current_state <= next_state;
end

always_comb begin
    out_in_reg = '0;
    out_mux_mask = '0;
    out_mask_reg = '0;
    out_out_reg = '0;
    case (current_state)
        IDLE: begin out_mux_mask = '1; out_in_reg = '1; end
        START: begin out_in_reg = '1; out_mux_mask = '1; out_mask_reg = '1; out_out_reg = '1; end
        GRANT: begin out_mask_reg = '1; out_out_reg = '1;  end
        default: begin
            out_in_reg = '0;
            out_mux_mask = '0;
            out_mask_reg = '0;
            out_out_reg = '0;
        end
    endcase
end

endmodule
`endif
