/*************************************************************************
Name of the module:
	SHIFT_R.sv
Description:
	This module is shift register
Version:
	1.0
Author:
	Alberto Vargas   <ie710231@iteso.mx>
	Isaac Vázquez 	<ie703092@iteso.mx>
Date: 
	17/09/2021
*************************************************************************/

module shift_l #
(
    parameter W_DATA = 8
)
(
	// Input - Value to be shifted if enb is set	
	input logic[W_DATA-1:0] 	datain,
	
	// Output - Shifted value to the right
	output logic[W_DATA-1:0] dataout
);
//	temporary register
logic[W_DATA-1:0] temp_register;

always_comb begin
    temp_register = datain << 1;
end

// Assign the temporary register to the output
assign dataout = temp_register;
	
endmodule
