`ifndef NOT_ZERO_SV
    `define NOT_ZERO_SV
module not_zero #
(
	parameter W_DATA = 8
)
(
	input[W_DATA-1:0] in_a,
	output logic out
);

logic tmp_out;

always_comb begin
    tmp_out = '0;
	for(int i = 0; W_DATA-1 > i ;i++)
		tmp_out |= in_a[i];
end

assign out = tmp_out;

endmodule
`endif
