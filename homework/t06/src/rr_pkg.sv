`ifndef RR_PKG_SV
	`define RR_PKG_SV
 package rr_pkg;

 localparam W_DATA = 8;

 typedef enum logic[1:0]{
    IDLE,
    START,
    GRANT,
    HOLD
 }rr_state_e;

 endpackage
`endif