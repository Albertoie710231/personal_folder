//Coder:          Abisai Ramirez Perez
//Description:    This module defines a parametric pipo register
module pipo #(
parameter DW = 4
) (
input  bit                clk,
input  bit                rst,
input  logic              enb,
input  logic              clean,
input  logic [DW-1:0]     inp,
output logic [DW-1:0]     out
);

logic [DW-1:0]      rgstr_r;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else if (enb)
        rgstr_r  <= inp;
    else if (clean)
        rgstr_r <= '0;

end:rgstr_label

assign out  = rgstr_r;

endmodule
