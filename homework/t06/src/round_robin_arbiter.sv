`ifndef ROUND_ROBIN_ARBITER_SV
	`define ROUND_ROBIN_ARBITER_SV

module round_robin_arbiter #
(
	parameter N_REQ = 8,
	parameter W_N_REQ = $clog2(N_REQ)
)
(
	input bit 			cclk,
	input bit 			arsnt_n,
	input logic[N_REQ-1:0] 		request,
	input logic			holdgrant,
	output logic			valid,
	output logic[W_N_REQ-1:0] 	grant
);

logic[N_REQ-1:0] w_in_reg;
logic[N_REQ-1:0] w_ppc_to_mask_reg;
logic[N_REQ-1:0] w_mask_reg_out;
logic[N_REQ-1:0] w_mux_first_ite_out;
logic[N_REQ-1:0] w_ppc_out;
logic[N_REQ-1:0] w_shift_ppc;
logic[W_N_REQ-1:0] w_out_dec;
logic w_sel_mask;

//rr_fsm rr_control
//(
    //.clk(cclk),
    //.rst(arsnt_n),
    //.mask(w_ppc_out),
    //.finished(w_sel_mask),
    //.in_hold_grant(holdgrant),
    //.request(request),
    //.out_in_reg(w_rr_con_in_reg),
    //.out_mux_mask(w_rr_con_mux_mask),
    //.out_mask_reg(w_rr_con_mask_reg),
    //.out_out_reg(w_rr_con_out_reg)
//);

not_zero #
(
    .W_DATA(N_REQ)
)
not_zero
(
    .in_a(request),
    .out(w_not_zero_out)
);

//pipo #
//(
    //.DW(N_REQ)
//)
//in_reg
//(
    //.clk(cclk),
    //.rst(arsnt_n),
    //.enb(w_not_zero_out && w_sel_mask && holdgrant),
    //.clean('0),
    //.inp(request),
    //.out(w_in_reg)
//);

pipo #
(
    .DW(N_REQ)
)
mask_reg
(
    .clk(cclk),
    .rst(arsnt_n),
    .enb(w_not_zero_out && holdgrant),
    .clean('0),
    .inp(w_shift_ppc),
    .out(w_mask_reg_out)
);

selector #
(
	.W_DATA(N_REQ)
)
sel_mask
(
    .in_a(w_mask_reg_out),
    .pin_sel(w_sel_mask)
);

mux_param mux_first_ite
(
    .datain({request,request & w_mask_reg_out}),     // Incoming data bus
    .select(w_sel_mask),     // Selector
    .sltd_o(w_mux_first_ite_out)
);

ppc_unit #
(
	.W_DATA(N_REQ)
)
ppc_unit
(
    .req_vect(w_mux_first_ite_out),
	.out_ppc(w_ppc_out)
);

shift_l shift_ppc
(
    .datain(w_ppc_out),
    .dataout(w_shift_ppc)
);

bin_dec position_dec
(
    .in_a(w_ppc_out),
    .out_dec(w_out_dec)
);

pipo #
(
    .DW(W_N_REQ)
)
out_reg
(
    .clk(cclk),
    .rst(arsnt_n),
    .enb(w_not_zero_out && holdgrant),
    .clean('0),
    .inp(w_out_dec),
    .out(grant)
);

assign valid = w_not_zero_out && arsnt_n;

endmodule
`endif
