`ifndef SELECTOR_SV
	`define SELECTOR_SV

module selector #
(
    parameter W_DATA = 8
)
(
    input[W_DATA-1:0] in_a,
    output logic pin_sel
);

always_comb begin
    if(in_a == '0)
        pin_sel = 1'b1;
    else
        pin_sel = 1'b0;
end

endmodule
`endif
