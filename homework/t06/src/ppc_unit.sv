`ifndef PPC_INIT_SV
	`define PPC_INIT_SV
module ppc_unit #
(
	parameter W_DATA = 8
)
(
	input[W_DATA-1:0] req_vect,
	output[W_DATA-1:0] out_ppc
);

logic[W_DATA-1:0] temp_out;

always_comb begin
    temp_out[0] = req_vect[0];
	for(int i = 1; i < W_DATA ;i++) begin
        temp_out[i] = temp_out[i-1] | req_vect[i];
	end
end

assign out_ppc = temp_out;

endmodule
`endif
