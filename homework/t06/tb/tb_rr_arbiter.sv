`timescale 1ns / 1ps

module tb_rr_arbiter;

import mux_pkg::*;

logic clk = 1;
logic rst = 0;
logic holdgrant = 0;
logic[7:0] vect_request;
logic[$clog2(8)-1:0] vect_grant;
logic valid;

round_robin_arbiter #
(
	.N_REQ(8)
) 
rr_ite
(
	.cclk		(clk),
	.arsnt_n	(rst),
	.request	(vect_request),
	.holdgrant	(holdgrant),
	.valid		(valid),
	.grant		(vect_grant)

);

always #1 clk <= ~clk;
always #18 vect_request <= $urandom()%256;
always begin
#2
holdgrant = '1;
#6
holdgrant = '0;
end

initial begin
	holdgrant = '1;
	vect_request = '0;
	rst = 0;
#2 	rst = 1;
    vect_request = '1;
end

endmodule
