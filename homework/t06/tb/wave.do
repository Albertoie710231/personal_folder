onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_rr_arbiter/clk
add wave -noupdate /tb_rr_arbiter/rst
add wave -noupdate /tb_rr_arbiter/vect_request
add wave -noupdate -radix unsigned /tb_rr_arbiter/vect_grant
add wave -noupdate /tb_rr_arbiter/valid
add wave -noupdate /tb_rr_arbiter/holdgrant
add wave -noupdate -divider {IN REG}
add wave -noupdate /tb_rr_arbiter/rr_ite/in_reg/enb
add wave -noupdate /tb_rr_arbiter/rr_ite/in_reg/inp
add wave -noupdate /tb_rr_arbiter/rr_ite/in_reg/out
add wave -noupdate -divider PPC
add wave -noupdate /tb_rr_arbiter/rr_ite/ppc_unit/req_vect
add wave -noupdate /tb_rr_arbiter/rr_ite/ppc_unit/out_ppc
add wave -noupdate -divider MASK
add wave -noupdate /tb_rr_arbiter/rr_ite/mask_reg/enb
add wave -noupdate /tb_rr_arbiter/rr_ite/mask_reg/inp
add wave -noupdate /tb_rr_arbiter/rr_ite/mask_reg/out
add wave -noupdate -divider DEC
add wave -noupdate /tb_rr_arbiter/rr_ite/position_dec/in_a
add wave -noupdate /tb_rr_arbiter/rr_ite/position_dec/out_dec
add wave -noupdate /tb_rr_arbiter/rr_ite/position_dec/temp_pos
add wave -noupdate -divider {NOT ZERO}
add wave -noupdate /tb_rr_arbiter/rr_ite/not_zero/in_a
add wave -noupdate /tb_rr_arbiter/rr_ite/not_zero/out
add wave -noupdate -divider {OUT REG}
add wave -noupdate /tb_rr_arbiter/rr_ite/out_reg/enb
add wave -noupdate /tb_rr_arbiter/rr_ite/out_reg/inp
add wave -noupdate /tb_rr_arbiter/rr_ite/out_reg/out
add wave -noupdate -divider {ZERO MASK}
add wave -noupdate /tb_rr_arbiter/rr_ite/zero_mask/in_a
add wave -noupdate /tb_rr_arbiter/rr_ite/zero_mask/pin_sel
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {15173 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 316
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {28238 ps}
