`ifndef UART_PKG_SV
	`define UART_PKG_SV

package uart_pkg;

typedef enum logic[3:0]
{
	IDLE,
	RX_START_BIT,
	RX_DATA_BITS,
	RX_STOP_BIT,
	CLEANUP
}uart_state_e;

endpackage
`endif
