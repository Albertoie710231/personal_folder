`ifndef UART_IF_SV
	`define IAR_IF_SV 

interface uart_if();

logic sd_rx;
logic rst;
logic clear_int;
logic transmit;
logic[7:0] received_data;
logic[7:0] data_to_transmit;
logic rx_int;
logic parity_error;
logic serial_out_tx;

modport receiver (
	input sd_rx,
	input rst,
	input clear_int,
	input transmit,
	input received_data,
	output data_to_transmit,
	output rx_int,
	output parity_error,
	output serial_out_tx

);

modport transmiter (
	output sd_rx,
	output rst,
	output clear_int,
	output transmit,
	output received_data,
	input data_to_transmit,
	input rx_int,
	input parity_error,
	input serial_out_tx
);

endinterface
`endif
