module top_uart
import uart_pkg::*;
(
	input logic clk,
	uart_if.receiver uart_if
);

uart_state_e current_state, nxt_state;

always_comb begin
	case(current_state)
		IDLE: begin
			if('0 == uart_if.sd_rx)
				nxt_state = RX_START_BIT;
			else
				nxt_state = IDLE;
		end
		RX_START_BIT: begin
			if('0 == uart_if.sd_rx)
				nxt_state = RX_DATA_BITS;
			else
				nxt_state = IDLE;
		end
		RX_DATA_BITS: begin
			if('0 == uart_if.sd_rx)
				nxt_state = IDLE;
			else
				nxt_state = RX_DATA_BITS;
		end
		RX_STOP_BIT: begin
			if('0 == uart_if.sd_rx)
				nxt_state = IDLE;
			else
				nxt_state = RX_DATA_BITS;
		end
		CLEANUP: begin
			if('0 == uart_if.sd_rx)
				nxt_state = IDLE;
			else
				nxt_state = RX_DATA_BITS;
		end

		default:begin
			nxt_state = IDLE;
		end
	endcase
end

always @(posedge clk or negedge uart_if.rst) begin
	if(!uart_if.rst)
		current_state <= IDLE;
	else
		current_state <= nxt_state; 
end

always_comb begin
	uart_if.data_to_transmit = '0;
	uart_if.rx_int = '0;
	uart_if.parity_error = '0;
	uart_if.serial_out_tx = '0;
	
	case(current_state)
		IDLE: begin
			uart_if.data_to_transmit = '0;
			uart_if.rx_int = '0;
			uart_if.parity_error = '0;
			uart_if.serial_out_tx = '0;
		end
		RX_START_BIT: begin
			uart_if.data_to_transmit = '0;
			uart_if.rx_int = '0;
			uart_if.parity_error = '0;
			uart_if.serial_out_tx = '0;
		end
		RX_DATA_BITS: begin
			uart_if.data_to_transmit = '0;
			uart_if.rx_int = '0;
			uart_if.parity_error = '0;
			uart_if.serial_out_tx = '0;
		end
		RX_STOP_BIT: begin
			uart_if.data_to_transmit = '0;
			uart_if.rx_int = '0;
			uart_if.parity_error = '0;
			uart_if.serial_out_tx = '0;
		end
		CLEANUP: begin
			uart_if.data_to_transmit = '0;
			uart_if.rx_int = '0;
			uart_if.parity_error = '0;
			uart_if.serial_out_tx = '0;
		end
		default: begin
			uart_if.data_to_transmit = '0;
			uart_if.rx_int = '0;
			uart_if.parity_error = '0;
			uart_if.serial_out_tx = '0;
		end
	endcase
end

endmodule
