/*************************************************************************
Nombre del modulo:
BCD_to_seg_pkg.sv
Descripcion:
Aqui se declaran los parametros con los que trabajara en todos los 
submodulos
Version:
1.0
Autor:
Alberto Vargas Garrido - 710231
Fecha:
03/10/21
*************************************************************************/
`ifndef BCD_TO_SEG_PKG
	`define BCD_TO_SEG_PKG

package BCD_to_seg_pkg;
	typedef enum logic [6:0]{

		ZERO  = 7'b0000001,
		ONE   = 7'b1001111,
		TWO   = 7'b0010010,
		THREE = 7'b0000110,
		FOUR  = 7'b1001100,
		FIVE  = 7'b0100100,
		SIX   = 7'b0100000,
		SEVEN = 7'b0001111,
		EIGHT = 7'b0000000,
		NINE  = 7'b0001100

	}e_segment;

endpackage: BCD_to_seg_pkg
`endif
