/*************************************************************************
Nombre del modulo:
top_dec_bin.sv
Descripcion:
En este modulo encapsula los modulos del decodificador binario a BCD y BCD al display de 7 segmentos.
Entradas:
El binario introducido es un numero de 8 bits.
Salidas:
Las salidas correponden a los digitos extraidos del numero de entrada. Son de 7 bits.
Version:
1.0
Autor:
Alberto Vargas Garrido
Fecha:
03/10/21
*************************************************************************/

module top_dec_bin (
	input logic [7:0] in_binary,
	output logic [6:0] out_segments_units,
	output logic [6:0] out_segments_tens,
	output logic [6:0] out_segments_hundr,
	output logic out_sign
);

	logic [3:0] units_to_seg_w;
	logic [3:0] tens_to_seg_w;
	logic [3:0] hundr_to_seg_w;

	dec_bin_BCD dec2bcd
	(
	.binary		(in_binary),
	.units		(units_to_seg_w),
	.tens		(tens_to_seg_w),
	.hundreds	(hundr_to_seg_w),
	.sign		(out_sign)
	);

	BCD_to_seg seg_units
	(
	.in_bcd		(units_to_seg_w),
	.out_segments	(out_segments_units)
	);
	
	BCD_to_seg seg_tens
	(
	.in_bcd		(tens_to_seg_w),
	.out_segments	(out_segments_tens)
	);

	BCD_to_seg seg_hundr
	(
	.in_bcd		(hundr_to_seg_w),
	.out_segments	(out_segments_hundr)
	);

endmodule
