/*************************************************************************
Nombre del modulo:
BCD_to_seg.sv
Descripcion:
En este modulo se hara la conversion del numero binario para cada segmento
con sus valores correspondientes para mostrarlos en el display
Entradas:
El binario correspondiente del 0 al 9
Salidas:
Valor binario para lo segmentos del display
Version:
1.0
Autor:
Alberto Vargas Garrido - 710231
Fecha:
03/10/21
*************************************************************************/
import BCD_to_seg_pkg::*;

module BCD_to_seg (
	input logic [3:0] in_bcd,
	output logic [6:0] out_segments 
);

always @ (in_bcd)
	case(in_bcd)
		4'd0:out_segments <= ZERO;
		4'd1:out_segments <= ONE;
		4'd2:out_segments <= TWO;
		4'd3:out_segments <= THREE;
		4'd4:out_segments <= FOUR;
		4'd5:out_segments <= FIVE;
		4'd6:out_segments <= SIX;
		4'd7:out_segments <= SEVEN;
		4'd8:out_segments <= EIGHT;
		4'd9:out_segments <= NINE;
		default:out_segments <= ZERO;
	endcase
endmodule
