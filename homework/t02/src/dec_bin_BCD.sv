/*************************************************************************
Nombre del modulo:
dec_bin_BCD.sv
Descripcion:
En este modulo se hara la decodificacion del numero binario introducido 
y se dividira en centenas, decenas y unidades. Incluyendo la conversion 
de complemento a 2.
Entradas:
El binario introducido y con el que se llevara todo el proceso para hacer
la decodificacion
Salidas:
Las salidas son los numeros correspondientes en binario para que sean convertidos
a los respectivos valores para los segmentos, incluyendo el signo.
Version:
1.0
Autor:
Alberto Vargas Garrido - 710231
Fecha:
03/10/2021
*************************************************************************/

module dec_bin_BCD (
	input logic [7:0] binary,
	output logic [3:0] units,
	output logic [3:0] tens,
	output logic [3:0] hundreds,
	output logic sign
);

logic [7:0] comp_2;
logic [7:0] temp_tens;
logic [7:0] temp_units, temp_hundr;

always_comb begin
	comp_2 = 0;

	comp_2 = (!binary[7]) ? binary : (~binary) + 1'b1;

	temp_units = comp_2 % 4'd10;

	/*
	* Al extraer los decimales es posible usar operador modulo con el
	* numero 9 solo de 10
	* a 80, sin embrago en el caso unico del 90 no funciona ya que nos da
	* 0.
	*/

	temp_tens = (7'd100 <= comp_2) ? comp_2 - 7'd100 - temp_units : comp_2 - temp_units;
	temp_tens = (7'd90 <= temp_tens) ? 4'd9 : temp_tens % 4'd9;
	
	temp_hundr = (7'd100 <= comp_2) ? 4'b0001:4'b0000;
end

assign units = temp_units[3:0];
assign tens = temp_tens[3:0];
assign hundreds = temp_hundr[3:0];
assign sign = ~binary[7];

endmodule
