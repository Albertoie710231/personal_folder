`timescale 1ns / 1ps

module tb_bin_to_seg;

import BCD_to_seg_pkg::*;

logic [7:0]	in_bin;
logic [6:0]	out_segments_units;
logic [6:0]	out_segments_tens;
logic [6:0]	out_segments_hundr;
logic 		sign;

top_dec_bin top
(
	.in_binary		(in_bin),
	.out_segments_units	(out_segments_units),
	.out_segments_tens	(out_segments_tens),
	.out_segments_hundr	(out_segments_hundr),
	.out_sign		(sign)
);

initial begin
	in_bin = -128;
	repeat (2**8) begin
		#2
		in_bin = in_bin + 1;
	end
	#500
	$stop;
	
end
endmodule
